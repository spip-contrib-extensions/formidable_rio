# Changelog
## 1.0.1 - 2024-07-19

### Fixed

- Compatible SPIP 4 et >

## 1.0.0 - 2022-05-31

### Added

- Compatibilité SPIP 4

### Removed

- Compatibilité SPIP 3.2 et 3.1

## 0.1.2 -2022-05-22

### Fixed

- Le formulaire ne s'affichait pas
