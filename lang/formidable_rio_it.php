<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de https://trad.spip.net/tradlang_module/formidable_rio?lang_cible=it
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// E
	'explication' => 'Facendo clic su questo pulsante si ripristina il valore per indicare che i campi sono obbligatori.',

	// T
	'texte_submit' => 'Ripristina'
);
